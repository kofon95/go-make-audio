module myportaudio

go 1.16

require (
	github.com/gordonklaus/portaudio v0.0.0-20200911161147-bb74aa485641
	github.com/karalabe/xgo v0.0.0-20191115072854-c5ccff8648a7 // indirect
	github.com/zenwerk/go-wave v0.0.0-20190102022600-1be84bfef50c
)
